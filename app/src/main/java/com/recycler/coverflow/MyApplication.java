package com.recycler.coverflow;

import com.recycler.coverflow.utils.PixelUtil;

import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        PixelUtil.initContext(this);
    }
}
