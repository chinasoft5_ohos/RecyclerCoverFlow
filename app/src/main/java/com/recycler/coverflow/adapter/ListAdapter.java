package com.recycler.coverflow.adapter;

import com.recycler.coverflow.ResourceTable;
import com.recycler.coverflow.recyclerview.MyListContainer;

import ohos.aafwk.ability.AbilitySlice;

import ohos.agp.components.*;

import java.util.List;

public class ListAdapter extends BaseItemProvider {
    private static List<Integer> list;
    private static AbilitySlice slice = null;

    private final static int TYPE_COVER_FLOW = 0;

    public ListAdapter(List<Integer> list, AbilitySlice slice) {
        this.list = list;
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component cpt = null;
        ViewHolder holder;
        if (cpt == null) {
            if (position == TYPE_COVER_FLOW) {
                cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_coverflow_ohos, null, false);
            } else {
                cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_text_ohos, null, false);
            }
            holder = new ViewHolder();
            if (position == TYPE_COVER_FLOW) {
                holder.listContainer = (MyListContainer) cpt.findComponentById(ResourceTable.Id_cover_flow);
            }
            holder.text = (Text) cpt.findComponentById(ResourceTable.Id_text);
            cpt.setTag(holder);
        } else {
            holder = (ViewHolder) cpt.getTag();
        }
        if (position == TYPE_COVER_FLOW) {
            holder.listContainer.setOrientation(Component.HORIZONTAL);
            intiCoverFlow(holder.listContainer, holder.text);
        } else {
            int sampleItem = list.get(position);
            holder.text.setText(String.valueOf(sampleItem));
        }
        return cpt;
    }

    private void intiCoverFlow(MyListContainer listContainer, Text text) {
        listContainer.setItemProvider(new Adapter(slice, false));
//        listContainer.setScrollListener(() -> {
//            if (listContainer.getIndex() == 0||listContainer.getIndex() % 6 == 0) {
//                text.setText(1 + "/6");
//            }else {
//                text.setText(listContainer.getIndex() % 6 + 1 + "/6");
//            }
//        });
        listContainer.setOnCurrentItemListener(posstion -> {
            int index = posstion - 2;
            if (index == 0 || index % 6 == 0) {
                text.setText(1 + "/6");
            } else {
                text.setText(index % 6 + 1 + "/6");
            }
        });
    }

    static class ViewHolder {
        MyListContainer listContainer;
        Text text;
    }


}
