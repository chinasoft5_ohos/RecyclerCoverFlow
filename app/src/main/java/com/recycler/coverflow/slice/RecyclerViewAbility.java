package com.recycler.coverflow.slice;

import com.recycler.coverflow.ResourceTable;
import com.recycler.coverflow.adapter.ListAdapter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.ListContainer;

import java.util.ArrayList;

public class RecyclerViewAbility extends AbilitySlice {

    private ListContainer mlist;
    private ArrayList<Integer> dataList = new ArrayList<>();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_recyclerview_ohos);

        mlist = (ListContainer) findComponentById(ResourceTable.Id_listContainer);

        initData();
        initList();
    }

    private void initData() {
        for (int i = 0; i <= 49; i++) {
            dataList.add(i);
        }
    }

    private void initList() {
        mlist.setItemProvider(new ListAdapter(dataList, this));
    }

}
