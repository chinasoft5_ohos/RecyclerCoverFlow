package com.recycler.coverflow.slice;

import com.recycler.coverflow.ResourceTable;
import com.recycler.coverflow.adapter.Adapter;
import com.recycler.coverflow.recyclerview.MyListContainer;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.*;

import java.util.ArrayList;

public class ViewPagerAbility extends AbilitySlice {
    private PageSlider pageSlider;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Component root = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_viewpager_ohos, null, false);
        setUIContent((ComponentContainer) root);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_xml_view_pager);
        initPageSlider();
        initPageSliderIndicator();
    }

    private void initPageSliderIndicator() {
    }

    private void initPageSlider() {
        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        DirectionalLayout layoutOne = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_ohos, null, false);
        DirectionalLayout layoutTwo = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_ohos, null, false);
        DirectionalLayout layoutThree = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_ohos, null, false);
        DirectionalLayout layoutFour = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_ohos, null, false);
        DirectionalLayout layoutFive = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_ohos, null, false);

        ArrayList<Component> pageView = new ArrayList();
        pageView.add(layoutOne);
        pageView.add(layoutTwo);
        pageView.add(layoutThree);
        pageView.add(layoutFour);
//        pageView.add(layoutFive);
        pageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return pageView.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(pageView.get(i));
                MyListContainer mList = (MyListContainer) pageView.get(i).findComponentById(ResourceTable.Id_list);
                Text textStr = (Text) pageView.get(i).findComponentById(ResourceTable.Id_text);
                Adapter adapter = new Adapter(getContext(), null, false);

//                mList.setScrollListener(() -> {
//                    if (mList.getIndex() == 0 || mList.getIndex() % 6 == 0) {
//                        textStr.setText(1 + "/6");
//                    } else {
//                        textStr.setText(mList.getIndex() % 6 + 1 + "/6");
//                    }
//                });
                mList.setOnCurrentItemListener(posstion -> {
                    int index = posstion - 2;
                    if (index == 0 || index % 6 == 0) {
                        textStr.setText(1 + "/6");
                    } else {
                        textStr.setText(index % 6 + 1 + "/6");
                    }
                });


                mList.setItemProvider(adapter);
                mList.setOrientation(Component.HORIZONTAL);

                return pageView.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(pageView.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        pageSlider.setCurrentPage(0);
    }

}
