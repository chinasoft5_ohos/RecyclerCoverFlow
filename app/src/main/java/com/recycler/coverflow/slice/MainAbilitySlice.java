package com.recycler.coverflow.slice;

import com.recycler.coverflow.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main_ohos);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);

        Component on3DCoverFlowClick = findComponentById(ResourceTable.Id_on3DCoverFlowClick);
        Component onJustCoverFlowClick = findComponentById(ResourceTable.Id_onJustCoverFlowClick);
        Component onRecyclerViewClick = findComponentById(ResourceTable.Id_onRecyclerViewClick);
        Component onViewPagerClick = findComponentById(ResourceTable.Id_onViewPagerClick);

        on3DCoverFlowClick.setClickedListener(this::onClick);
        onJustCoverFlowClick.setClickedListener(this::onClick);
        onRecyclerViewClick.setClickedListener(this::onClick);
        onViewPagerClick.setClickedListener(this::onClick);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_on3DCoverFlowClick:
//                present(new CoverFlow3DAbility(), new Intent());
                new ToastDialog(getContext()).setText("不支持3D效果").show();
                break;
            case ResourceTable.Id_onJustCoverFlowClick:
                present(new JustCoverFlowAbility(), new Intent());
                break;
            case ResourceTable.Id_onRecyclerViewClick:
                present(new RecyclerViewAbility(), new Intent());
                break;
            case ResourceTable.Id_onViewPagerClick:
                present(new ViewPagerAbility(), new Intent());
                break;
        }
    }
}
