package com.recycler.coverflow.slice;

import com.recycler.coverflow.ResourceTable;
import com.recycler.coverflow.adapter.Adapter;
import com.recycler.coverflow.recyclerview.MyListContainer;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class JustCoverFlowAbility extends AbilitySlice implements Adapter.OnItemClick {
    private MyListContainer mList;
    private Text text;
    private Adapter adapter;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_just_coverflow_ohos);
        initView();
    }

    public void initView() {
        mList = (MyListContainer) findComponentById(ResourceTable.Id_list);
        text = (Text) findComponentById(ResourceTable.Id_index);
        adapter = new Adapter(this, this, true);
        mList.setItemProvider(adapter);
        mList.setOrientation(Component.HORIZONTAL);

//        mList.setScrollListener(() -> {
//            if (mList.getIndex() == 0 || mList.getIndex() % 6 == 0) {
//                text.setText(1 + "/6");
//            } else {
//                text.setText(mList.getIndex() % 6 + 1 + "/6");
//            }
//        });
        mList.setOnCurrentItemListener(posstion -> {
            if (posstion == 0 || posstion % 6 == 0) {
                text.setText(1 + "/6");
            } else {
                text.setText(posstion % 6 + 1 + "/6");
            }
        });
    }

    @Override
    public void clickItem(int pos) {
    }
}
