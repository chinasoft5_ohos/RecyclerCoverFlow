/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.recycler.coverflow;

import com.recycler.coverflow.utils.LogUtil;
import com.recycler.coverflow.utils.PixelUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.content.Intent;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 单元测试.
 */
public class ExampleOhosTest extends AbilitySlice {
    /**
     * 初始化.
     *
     * @param intent intent
     */
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        PixelUtil.initContext(this);
    }

    /**
     * 检查包名.
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.recycler.coverflow", actualBundleName);
    }

    /**
     * 检查获取屏幕宽度.
     */
    @Test
    public void testScreenWidth() {
        int width = PixelUtil.screenWidth();
        Assert.assertNotNull(width);
        LogUtil.loge(String.valueOf(width));
    }

    /**
     * 检查vp2px 计量转换.
     */
    @Test
    public void testVp2px() {
        double origin = 12.5F;
        double expected = 38F;
        double px = PixelUtil.vp2px(origin);
        Assert.assertNotNull(px);
        Assert.assertEquals(expected, px, 0);
    }

}