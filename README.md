# RecyclerCoverFlow
--------------------------
## 项目介绍
- 项目名称：RecyclerCoverFlow
- 所属系列：openharmony的第三方组件适配移植
- 功能：使用ListContainer自定义ListContainer实现旋转木马相册效果
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release v1.1.0

## 效果演示
![screen1](  /gif/RecyclerCoverFlow01.gif "屏幕截图.gif")
![screen1](  /gif/RecyclerCoverFlow02.gif "屏幕截图.gif")
![screen1](  /gif/RecyclerCoverFlow03.gif "屏幕截图.gif")



## 安装教程


1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
 dependencies {
 implementation('com.gitee.chinasoft_ohos:RecyclerCoverFlow:1.0.2')；
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
 XML

```
 <com.recycler.coverflow.recyclerview.MyListContainer
        ohos:id="$+id:list"
        ohos:height="match_content"
        ohos:width="match_parent"/>
```
 Programatically
```
	 MyListContainer mList= (MyListContainer) findComponentById(ResourceTable.Id_list);
	 Adapter adapter = new Adapter(this, this, true);
        mList.setItemProvider(adapter);
        mList.setOrientation(Component.HORIZONTAL);

```


## 测试信息

- CodeCheck代码测试无异常

- CloudTest代码测试无异常

- 病毒安全检测通过

- 当前版本demo功能与原组件基本无差异

## 待实现

- CoverFlow3DAbility未实现

## 版本迭代
- 1.0.2
- 1.0.1
- 1.0.0
- 0.0.1-SNAPSHOT

## 版权和许可信息

- Apache-2.0 License
