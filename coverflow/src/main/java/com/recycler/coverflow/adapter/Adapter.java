/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.recycler.coverflow.adapter;

import com.recycler.coverflow.ResourceTable;
import com.recycler.coverflow.utils.PixelUtil;

import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;

import ohos.app.Context;

import ohos.global.resource.Resource;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends BaseItemProvider {
    static int screenItemCount = 5;

    private final Context mContext;
    private List<ViewHolder> mTotals = new ArrayList<>();
    private List<PixelMapElement> mColors = new ArrayList<>();
    private OnItemClick clickCb;

    /**
     * 是否无限循环.
     */
    private boolean mIsLoop = false;

    public Adapter(Context context, boolean isLoop) {
        this(context, null, isLoop, null);
    }

    public Adapter(Context context, OnItemClick itemClick, boolean isLoop) {
        this(context, itemClick, isLoop, null);
    }

    public Adapter(Context context, OnItemClick itemClick, boolean isLoop, List datas) {
        mContext = context;
        clickCb = itemClick;
        this.mIsLoop = isLoop;
        if (datas == null) {
            createDefaultPictures();
        }
    }

    private void createDefaultPictures() {
        if (!mIsLoop) {
            //解决非循环问题不能滑到边，下面的size加4
            createEmptyPicture();
            createEmptyPicture();
        }
        createPicture(ResourceTable.Media_item1);
        createPicture(ResourceTable.Media_item2);
        createPicture(ResourceTable.Media_item3);
        createPicture(ResourceTable.Media_item4);
        createPicture(ResourceTable.Media_item5);
        createPicture(ResourceTable.Media_item6);
        if (!mIsLoop) {
            //解决非循环问题不能滑到边，下面的size加4
            createEmptyPicture();
            createEmptyPicture();
        }
//        createPicture(ResourceTable.Media_item1);
//        createPicture(ResourceTable.Media_item2);
//        createPicture(ResourceTable.Media_item3);
//        createPicture(ResourceTable.Media_item4);
//        createPicture(ResourceTable.Media_item5);
//        createPicture(ResourceTable.Media_item6);
    }

    private void createPicture(int resId) {
        try {
            Resource resource = mContext.getResourceManager().getResource(resId);
            PixelMapElement element = new PixelMapElement(resource);
            mColors.add(element);
            onCreateViewHolder();
        } catch (Exception ignored) {
        }
    }

    public void createEmptyPicture() {
        mColors.add(null);
        onCreateViewHolder();
    }

    /**
     * 获取条目总数.
     *
     * @return 获取条目总数
     */
    @Override
    public int getCount() {
        return mIsLoop ? (Integer.MAX_VALUE / 2) : 6 + 4;
    }

    /**
     * 获取条目内容.
     *
     * @param position 当前index
     * @return 获取条目内容
     */
    @Override
    public Object getItem(int position) {
        return mColors.get(position);
    }

    /**
     * 获取条目Component.
     *
     * @param position 当前index
     * @return 获取条目Component
     */
    public Component getItemComponent(int position) {
        return mTotals.get(position % mColors.size()).itemView;
    }

    /**
     * 获取当前index.
     *
     * @param position 当前index
     * @return 获取当前index
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer parent) {
        ViewHolder holder = mTotals.get(position % mTotals.size());
        onBindViewHolder(holder, position);
        return holder.itemView;
    }

    /**
     * 条目点击监听.
     *
     * @param cb 条目点击监听
     */
    public void setOnClickLstn(OnItemClick cb) {
        this.clickCb = cb;
    }

    private ViewHolder onCreateViewHolder() {
        int layout = ResourceTable.Layout_layout_item;
        Component itemView = LayoutScatter.getInstance(mContext).parse(layout, null, false);
        itemView.setLayoutConfig(generateDefaultLayoutConfig());
        ViewHolder holder = new ViewHolder(itemView);
        itemView.setTag(holder);
        mTotals.add(holder);
        return holder;
    }

    private void onBindViewHolder(ViewHolder holder, final int position) {
        PixelMapElement pixelMapElement = mColors.get(position % mColors.size());
        if (pixelMapElement == null) {
            holder.itemView.setAlpha(0);
        } else {
            holder.itemView.setAlpha(255);
            holder.img.setImageElement(pixelMapElement);
            holder.itemView.setClickedListener(component -> {
                if (clickCb != null) {
                    clickCb.clickItem(position);
                }
            });
        }

    }

    private ComponentContainer.LayoutConfig generateDefaultLayoutConfig() {
        return new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
    }

    /**
     * 添加新数据.
     *
     * @param container ListContainer
     * @param width     原始宽
     */
    public void addNewData(ListContainer container, int width) {
        if (!mIsLoop) return;
        int scrollSize = mColors.size() < 7 ? mColors.size() : mTotals.size();
        container.scrollBy(scrollSize * width, 0);
        notifyDataChanged();
    }

    /**
     * 条目点击监听.
     */
    public interface OnItemClick {
        void clickItem(int pos);
    }

    static class ViewHolder {
        Component itemView;
        Image img;

        public ViewHolder(Component itemView) {
            this.itemView = itemView;
            this.img = (Image) itemView;
            int origin = PixelUtil.screenWidth() / screenItemCount;
            img.setLayoutConfig(new ComponentContainer.LayoutConfig(origin, (int) (origin * 1.5F)));
        }
    }
}
