/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.recycler.coverflow.utils;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;

import ohos.app.Context;

/**
 * 计量单位工具类.
 */
public final class PixelUtil {
    private static Display display;

    /**
     * 初始化.
     *
     * @param context 上下文
     */
    public static void initContext(Context context) {
        display = DisplayManager.getInstance().getDefaultDisplay(context).get();
    }

    /**
     * 返回屏幕宽度.
     *
     * @return 返回屏幕宽度
     */
    public static int screenWidth() {
        return display.getAttributes().width;
    }
    /**
     * 获取手机高
     *
     *
     * @return 返回手机高
     */
    public static int screenHeight() {
        return display.getAttributes().height;
    }
    /**
     * 计量单位转换fp2px.
     *
     * @param fp 输入计量
     * @return 返回单位
     */
    public static float fp2px(float fp) {
        float sca = display.getAttributes().scalDensity;
        return (int) (fp * sca + 0.5f * (fp >= 0 ? 1 : -1));
    }
    /**
     * 计量单位转换vp2px.
     *
     * @param vp 输入计量
     * @return 返回单位
     */
    public static float vp2px(float vp) {
        float dpi = display.getAttributes().densityPixels;
        return (int) (vp * dpi + 0.5f * (vp >= 0 ? 1 : -1));
    }
}
